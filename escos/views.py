from django import forms
from django.contrib.auth.forms import UserCreationForm, UserModel
from django.shortcuts import redirect, render
import os
from .forms import FormUser,TareaForm
from .models import Tarea,Usuario

#create your views here.
def inicio_cesion(request):
  #  print(print(os.path.dirname(os.path.realpath(__file__))))
    return render(request,'init_session.html',{
        "title":'bienvenido'})

def inicio(request):
  tareas =Tarea.objects.all()
  
  return render(request,'inicio.html',
  {"title":"inicio",
  "task":tareas})

def agregar(request):
 
  form=FormUser(request.POST)
  context={'form':form,"title":"registro"}
  if form.is_valid:
    form.save
  if  not form.is_valid:
    print("el formulario no es valido")
  return render(request,'registrer.html',context)
def agregarTarea(request):
  if request.method == "POST":
    form=TareaForm(request.POST)
    if form.is_valid():
      form.save()
      return redirect('/')
  else:
    form=TareaForm()
  context={'form':form}
  return render(request,'add.html',context)

def eliminarTarea(request,tarea_id):
 tareas=Tarea.objects.filter(id=tarea_id).delete()

 return redirect("/")

def editarTarea(request,tarea_id):
  tareas=Tarea.objects.get(id=tarea_id)
  if request.method=="POST":
    form=TareaForm(request.POST,instance=tareas)
    if form.is_valid():
      form.save()
      return redirect("/")
  else:
    form=TareaForm(instance=tareas)
  context ={"form":form,"title":"editar"}
  return render(request,"edit.html",context)