from django import forms
from django.forms import ModelForm, fields, models
from .models import Usuario ,Tarea
import datetime
from django.contrib.auth.forms import UserCreationForm
class FormUser(forms.ModelForm):
    nombre=forms.CharField(label="nombre", max_length=200, required=True)
    apellido=forms.CharField(label="apellido",max_length=200,required=True)
    fecha_nac=forms.DateField(label="fecha de nacimiento")
    email=forms.EmailField(label="email", required=True,max_length=255)
    passwd=forms.CharField(widget=forms.PasswordInput())
    class Meta:
            models =Usuario
            fields = ['__all__']

class TareaForm(forms.ModelForm):

    class Meta:
        model=Tarea
        fields = ['tarea']
 