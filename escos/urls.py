"""escos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from escos import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('incio de sesion/',views.inicio_cesion,name="indentificarse"),
    path('registro/',views.agregar,name="registro"),
    path('',views.inicio,name="init"),
    path('agregar/',views.agregarTarea,name="addTask"),
    path('eliminar/<int:tarea_id>/',views.eliminarTarea,name="deleteTask"),
    path('editar/<int:tarea_id>/',views.editarTarea,name="editTask")
]
