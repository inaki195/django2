from typing import DefaultDict
from django.db import models
import datetime

class Usuario(models.Model):
    nombre=models.CharField(max_length=200,null=False)
    apellido=models.CharField(max_length=200,null=False)
    fecha_nacimiento=models.DateField()
    email=models.EmailField( max_length=255,null=False)
    passwd=models.CharField(max_length=300,null=False) 

class Tarea(models.Model):
    tarea=models.CharField(max_length=100)
    def __str__(self):
        return self.tarea